#ifndef TENSOR_SLICING_H
#define TENSOR_SLICING_H

#include<cstdlib>
#include<string>
#include <cassert>

#include"indexpack/indexpack.h"
#include"matrix/mat_lazy.h"

using TensorF=function<cx_double(vector<int>)>;
using tensorBool=function<bool(MultiIndex)>;

inline vector<int> to_vec(MultiIndex const& m1,MultiIndex const& m2)
{
    vector<int> x(m1.begin(),m1.end());
    x.insert(x.end(),m2.begin(),m2.end());
    return x;
}

inline MatLazy TensorToMatfun(const TensorF& f,const IndexPack& Iset, const IndexPack& Jset)
{
    auto fm=[=](int i,int j) { return f( to_vec(Iset.at(i),Jset.at(j)) );  };
    return MatLazy(fm,Iset.size(),Jset.size());
}

inline MatLazy TensorToMatfunAdd(const MatLazy& A, const TensorF& f,const IndexPack& Iset, const IndexPack& Jset,
                                const IndexPack& Iset0, const IndexPack& Jset0)
{
    auto fm=[=](int i,int j) { return f( to_vec(Iset.at(i),Jset.at(j)) );  };
    auto A2=MatLazy(fm,Iset.size(),Jset.size());
    auto a=Iset.pos(Iset0);
    auto b=Jset.pos(Jset0);
    A2.A.submat(conv_to<uvec>::from(a),conv_to<uvec>::from(b))=A.A;
    return A2;
}

inline umat TensorToMatrixBool(const tensorBool& f,const IndexPack& Iset, const IndexPack& Jset)
{
    umat A(Iset.size(),Jset.size());
#pragma omp parallel for collapse(2)
    for(size_t i=0;i<Iset.size();i++)
        for(size_t j=0;j<Jset.size();j++)
            A(i,j)=f( Iset.at(i)+Jset.at(j) );
    return A;
}

inline umat TensorToMatrixBoolAdd(const umat& A, const tensorBool& f,const IndexPack& Iset, const IndexPack& Jset,
                                const IndexPack& Iset0, const IndexPack& Jset0)
{
    auto a=Iset.pos(Iset0);
    auto b=Jset.pos(Jset0);
    umat A2(Iset.size(), Jset.size());
    A2.fill(10);
    A2.submat(conv_to<uvec>::from(a),conv_to<uvec>::from(b))=A;
    uvec ids=find(A2>1);
#pragma omp parallel for
    for(size_t id:ids)
        A2(id)=f( Iset.at(id%A2.n_rows)+Jset.at(id/A2.n_rows) ); // <--- this  is for col-major only
    return A2;
}


inline cx_mat TensorToMatrix(MatLazy& f4,const IndexPack& Iset2,const IndexPack& Iset
                             , const IndexPack& In1, const IndexPack& In2,
                            const IndexPack& Jset, const IndexPack& Jset2)
{
    cx_mat A(Iset.size(),Jset.size());
    vector<int> Iid=kron(Iset2,In1).pos(Iset);
    vector<int> Jid=kron(In2,Jset2).pos(Jset);

    for(size_t i=0;i<Iset.size();i++)
        for(size_t j=0;j<Jset.size();j++)
            A(i,j)=f4(Iid.at(i),Jid.at(j));
    return A;
}


inline cx_cube Tensor4ToCube(MatLazy& f4,const IndexPack& Iset
                            , const IndexPack& In1, const IndexPack& In2,
                            const IndexPack& Jset, const IndexPack& Jset2)
{
    cx_cube A(Iset.size(),Jset.size(),In1.size());
    vector<int> Jid=kron(In2,Jset2).pos(Jset);

    for(size_t s=0;s<In1.size();s++)
        for(size_t i=0;i<Iset.size();i++)
            for(size_t j=0;j<Jset.size();j++)
                A.slice(s)(i,j)=f4(i*In1.size()+s,Jid.at(j)); // <------ sensitive to the definition of kron()
    return A;
}


inline cx_cube UpdateTensor4FromCube(MatLazy& f4,const IndexPack& Iset
                            , const IndexPack& In1, const IndexPack& In2,
                            const IndexPack& Jset, const IndexPack& Jset2,
                                     const cx_cube& A)
{
    vector<int> Jid=kron(In2,Jset2).pos(Jset);
    for(size_t s=0;s<In1.size();s++)
        for(size_t i=0;i<Iset.size();i++)
            for(size_t j=0;j<Jset.size();j++)
                f4.A(i*In1.size()+s,Jid.at(j))=A.slice(s)(i,j); // <------ sensitive to the definition of kron()
    return A;
}


inline cx_cube Tensor4ToCubeRight(MatLazy& f4,const IndexPack& Iset2,const IndexPack& Iset
                                 , const IndexPack& In1, const IndexPack& In2,
                                 const IndexPack& Jset)
{
    cx_cube A(Iset.size(),Jset.size(),In2.size());
    vector<int> Iid=kron(Iset2,In1).pos(Iset);

    for(size_t s=0;s<In2.size();s++)
        for(size_t i=0;i<Iset.size();i++)
            for(size_t j=0;j<Jset.size();j++)
                A.slice(s)(i,j)=f4(Iid.at(i),s*Jset.size()+j);  // <------ sensitive to the definition of kron()
    return A;
}


inline cx_cube UpdateTensor4FromCubeRight(MatLazy& f4,const IndexPack& Iset2,const IndexPack& Iset
                                 , const IndexPack& In1, const IndexPack& In2,
                                 const IndexPack& Jset,
                                          const cx_cube& A)
{
    vector<int> Iid=kron(Iset2,In1).pos(Iset);

    for(size_t s=0;s<In2.size();s++)
        for(size_t i=0;i<Iset.size();i++)
            for(size_t j=0;j<Jset.size();j++)
                f4.A(Iid.at(i),s*Jset.size()+j)=A.slice(s)(i,j);  // <------ sensitive to the definition of kron()
    return A;
}


/// B(is,j)=A(i,j,s) where s is the slice index
inline cx_mat CubeReorder(cx_cube const& A)
{
    cx_mat B(A.n_rows*A.n_slices,A.n_cols, fill::none);
    for(size_t j=0;j<A.n_cols;j++)
        for(size_t s=0;s<A.n_slices;s++)
            for(size_t i=0;i<A.n_rows;i++)
                B(i+s*A.n_rows,j)=A.slice(s)(i,j);
    return B;
}

/// A(i,j,s)=B(is,j) where s is the slice index
inline cx_cube CubeReorderBack(cx_mat const& B, size_t nrows)
{
    cx_cube A(nrows,B.n_cols,B.n_rows/nrows, fill::none);
    for(size_t j=0;j<A.n_cols;j++)
        for(size_t s=0;s<A.n_slices;s++)
            for(size_t i=0;i<A.n_rows;i++)
                A.slice(s)(i,j)=B(i+s*A.n_rows,j);
    return A;
}

inline cx_mat integrateUsing(const cx_cube& fA, const vector<double>& rule)
{
    cx_mat A(fA.n_rows,fA.n_cols,fill::zeros);
    for(size_t k=0;k<rule.size();k++)
        A+=fA.slice(k)*rule[k];
    return A;
}

inline cx_mat integrateUsing(const cx_cube& fA, const vector<cx_double>& rule)
{
    cx_mat A(fA.n_rows,fA.n_cols,fill::zeros);
    for(size_t k=0;k<rule.size();k++)
        A+=fA.slice(k)*rule[k];
    return A;
}

#endif // TENSOR_SLICING_H
