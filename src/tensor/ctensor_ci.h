#ifndef CTENSOR_CI_H
#define CTENSOR_CI_H

#include"tensor_ci.h"


using CTensorF=function<cx_double(vector<double>)>;

TensorF create_CTensorF_from(CTensorF f, vector<double> const& xi)
{
    return [=](vector<int> const& id)
    {
        vector<double> xs(id.size());
        for(size_t i=0;i<id.size();i++)
            xs[i]=xi[id[i]];
        return f(xs);
    };
}

struct CTensorCI
{
    vector<double> xi;
    CTensorF f;
    TensorCI ci;

    CTensorCI(int length, vector<double> xi_, CTensorF f_, vector<int> const& idRef={})
        : xi {xi_}
        , f {f_}
        , ci {length, int(xi_.size()), create_CTensorF_from(f,xi), idRef}
    {}

    void sweepHalf(){ ci.SweepHalf(); }
    double pivotErrorLastSweep() const { return ci.LastSweepPivotError(); }
    double errorEstimation(int nSampling) const { return ci.errorEstimation(nSampling); }
    cx_double integrateUsing(const vector<vector<double>>& weights) const { return ci.IntegrateUsing(weights); }

    cx_mat getT(int k, double x) const
    {
        auto const& Iset=convertIP(ci.Iid[k-1], xi);
        auto const& Jset=convertIP(ci.Jid[k+1], xi);
        cx_mat T(Iset.size(),Jset.size(),fill::none);
        for(size_t i=0;i<Iset.size();i++)
            for(size_t j=0;j<Jset.size();j++)
            {
                MultiIndexd id=Iset.at(i)+x+Jset.at(j);
                T(i,j)=f({id.begin(),id.end()});
            }
        return T;
    }

    cx_mat getTA(int k, double x) const { return ci.computeTAElem(getT(k,x), ci.A1[k]); }

    cx_double eval(vector<double> const& xs) const
    {
        if (xs.size()!=ci.length)
            throw invalid_argument("CTensorCI::eval(xs) input vector length != tensor length");
        auto prod=ci.one;
        for(size_t k=1;k<=ci.length;k++)
            prod=prod*getTA(k,xs[k-1]);
        return prod(0,0);
    }
};

#endif // CTENSOR_CI_H
