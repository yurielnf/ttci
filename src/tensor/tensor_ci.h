#ifndef TENSOR_CI_H
#define TENSOR_CI_H

#include<deque>
#include"tensor_slicing.h"
#include"matrix/matrix_ci.h"

//----------------------------- The class TensorCI ------------------

struct TensorCI
{
    size_t length;
    vector<IndexPack> Iid, Jid, locISetv;   //local index set
    vector<vector<MultiIndex>> Inew, Jnew;
    TensorF f;
    tensorBool fBool;
    PivotSearch pivotSearch;

    vector<MatLazy> Abig;
    vector<cx_cube> A;
    vector<cx_mat> A1;
    vector<umat> AbigBool;
    cx_mat one=cx_mat(1,1,fill::eye);

    double tol=1e-10, refValue=1.0, tol_factor=0;
    int tol_e=1;
    deque<double> pivotErrors;

    vector<vector<double>> rules;
    vector<cx_mat> w1,w2;

    TensorCI(){};
    TensorCI(int length,int localDim,TensorF f, vector<int> idRef={},tensorBool fBool={})
        : TensorCI(std::vector(length,localDim), f, idRef, fBool){}
    TensorCI(std::vector<int> localDim,TensorF f, vector<int> idRefv={},tensorBool fBool={})
        : length(localDim.size())
        , Iid(length+1), Jid(length+2), locISetv(length+1)
        , Inew(length+1), Jnew(length+2)
        , f(f)
        , fBool(fBool)
        , Abig(length+1), A(length+1), A1(length+1), AbigBool(length+1)
        , w1(length+1), w2(length+2)
    {
        if (idRefv.empty())
        for(size_t k=1;k<=length;k++) idRefv.push_back( localDim[k-1]/2 );//rand()%localDim;
        refValue=abs(f(idRefv));
        auto idRef=MultiIndex(idRefv.begin(),idRefv.end());
        for(size_t k=0;k<=length;k++)
        {
            Iid[k].push_back( idRef.substr(0,k) );
            Jid[k+1].push_back( idRef.substr(k,length-k) );
            if (k>0)
            for(int i=0;i<localDim[k-1];i++) locISetv[k].push_back({i});
        }
        for(size_t k=1;k<length;k++)
        {
            auto Iset=kron(Iid[k-1],locISetv[k]);
            auto Jset=kron(locISetv[k+1],Jid[k+2]);
            Abig[k]=TensorToMatfun(f, Iset, Jset);
            Abig[k].prepareEvalCross(
                        conv_to<uvec>::from( Iset.pos(Iid[k]) ),
                        conv_to<uvec>::from( Jset.pos(Jid[k+1]) ));
            if (fBool)
                AbigBool[k]=TensorToMatrixBool(fBool, Iset, Jset);
            A[k]=Tensor4ToCube(Abig[k],Iid[k-1],locISetv[k],locISetv[k+1],Jid[k+1],Jid[k+2]);
            if (k==length-1)
                A[k+1]=Tensor4ToCubeRight(Abig[k],Iid[k-1],Iid[k],locISetv[k],locISetv[k+1],Jid[k+2]);
            A1[k]=TensorToMatrix(Abig[k],Iid[k-1],Iid[k],locISetv[k],locISetv[k+1],Jid[k+1],Jid[k+2]);
        }
        A1[length]=one;
        pivotSearch = fBool ? PivotSearch::alternateWithCondition : PivotSearch::alternate;
        pivotErrors.push_back(refValue);

        w1[0]=w2[length+1]=one;

    }

    void setIntegrationRules(vector<vector<double>> const& rules)
    {
        this->rules=rules;
        fwd=false;
        for(size_t k=length-1;k>0;k--)
            UpdateWeights(k);
        fwd=true;
        pivotErrors[0]=refValue=abs(integrate());
    }

    void Iterate(int k)
    {
        if (Iid[k-1].size()*locISetv[k].size()==Iid[k].size() ||
            locISetv[k+1].size()*Jid[k+2].size()==Jid[k+1].size() )
            return;
        auto Iset=kron(Iid[k-1],locISetv[k]);
        auto Jset=kron(locISetv[k+1],Jid[k+2]);
        auto ci=MatrixCI(Abig[k], Iset.pos(Iid[k]), Jset.pos(Jid[k+1]));
        ci.pivotSearch=pivotSearch;
        ci.fBool=AbigBool[k];
        if (!rules.empty()) {
            ci.w1=kron(w1[k-1].as_col(), vec(rules[(k-1)%rules.size()]));
            ci.w2=kron(vec(rules[k%rules.size()]),w2[k+2].as_col());
        }

        auto piv=ci.FindNewPivot();

        pivotErrors.push_back(piv.error);
        while (pivotErrors.size()>=length) pivotErrors.pop_front();
        auto tol2=max(tol*refValue,pow(tol_factor,tol_e)*LastSweepPivotError());
        if (piv.error<=tol2) return;
        Inew[k].push_back(Iset.at(piv.i));
        Jnew[k+1].push_back(Jset.at(piv.j));
    }

    void ComputeNewAbig(int k)
    {
        auto I1=Iid[k-1];
        auto J1=Jid[k+2];
        for(auto x:Inew[k-1]) I1.push_back(x);
        for(auto x:Jnew[k+2]) J1.push_back(x);
        auto Iset=kron(I1,locISetv[k]);
        auto Jset=kron(locISetv[k+1],J1);
        auto Iset0=kron(Iid[k-1],locISetv[k]);
        auto Jset0=kron(locISetv[k+1],Jid[k+2]);
        Abig[k]=TensorToMatfunAdd(Abig[k],f, Iset,Jset,Iset0,Jset0);
        if(fBool)
            AbigBool[k]=TensorToMatrixBoolAdd(AbigBool[k],fBool,Iset,Jset,Iset0,Jset0);
    }
    void ComputeNewAbigAndNeighbors(size_t k)
    {
        auto Iset=kron(Iid[k-1],locISetv[k]);
        auto Jset=kron(locISetv[k+1],Jid[k+2]);
        Abig[k].prepareEvalCross(
                    conv_to<uvec>::from( Iset.pos(Inew[k]) ),
                    conv_to<uvec>::from( Jset.pos(Jnew[k+1]) ));

        if (k>1) ComputeNewAbig(k-1);
        if (k<length-1) ComputeNewAbig(k+1);
        UpdatePivots(k);

        //            A[k]=TensorToCube(f,Iid[k-1],locISet,Jid[k+1]);
        A[k]=Tensor4ToCube(Abig[k],Iid[k-1],locISetv[k],locISetv[k+1],Jid[k+1],Jid[k+2]);
        A[k+1]=Tensor4ToCubeRight(Abig[k],Iid[k-1],Iid[k],locISetv[k],locISetv[k+1],Jid[k+2]);
        //A1[k]=TensorToMatrix(f,Iid[k],Jid[k+1]);
        if (k<length)
            A1[k]=TensorToMatrix(Abig[k],Iid[k-1],Iid[k],locISetv[k],locISetv[k+1],Jid[k+1],Jid[k+2]);

        // now send the new info to the neighbors
        //from A[k+1] to Pi[k+1]
        if(k<=length-2)
            UpdateTensor4FromCube(Abig[k+1],Iid[k],locISetv[k+1],locISetv[k+2],Jid[k+2],Jid[k+3],A[k+1]);
        //from A[k] to Pi[k-1]
        if(k>=2)
            UpdateTensor4FromCubeRight(Abig[k-1],Iid[k-2],Iid[k-1],locISetv[k-1],locISetv[k],Jid[k+1],A[k]);

    }
    void UpdatePivots(int k)
    {
        for(auto x:Inew[k]) Iid[k].push_back(x);
        for(auto x:Jnew[k+1]) Jid[k+1].push_back(x);
        Inew[k].clear(); Jnew[k+1].clear();
    }

private:
    static cx_cube evalLocalAll_AT(cx_mat const& A1k,cx_cube const& Tk)
    {
        auto const Ak=cx_mat((cx_double *)Tk.memptr(),Tk.n_rows,Tk.n_cols*Tk.n_slices,false, true); //reshape T(i;js)
        cx_mat Ak2=join_cols(Ak.st(),A1k.st()), Qk,R;
        arma::qr_econ(Qk, R, Ak2);
        cx_mat Qk2=Qk.rows(0,Ak.n_cols-1);
        cx_mat Qk3=Qk.rows(Ak.n_cols,Qk.n_rows-1);
        cx_mat Tx=inv(Qk3).st()*Qk2.st();
        return cx_cube(Tx.memptr(),Tk.n_rows,Tk.n_cols,Tk.n_slices);
    }

public:
    void UpdateWeights(int k)
    {
        if (rules.empty()) return;
        if  (fwd) {
            auto const& myrule=rules[(k-1)%rules.size()];
            cx_cube TA=computeTA(A[k],A1[k]);
            w1[k]=w1[k-1]*integrateUsing(TA,myrule);
        }
        else {
            auto const& myrule=rules[k%rules.size()];
            cx_cube AT=evalLocalAll_AT(A1[k],A[k+1]);
            w2[k+1]=integrateUsing(AT,myrule)*w2[k+2];
        }
    }

    bool fwd=true;
    void SweepHalf()
    {
        vector<size_t> pos;
        if (fwd) for(size_t k=1;k<length;k++) pos.push_back(k);
        else for(size_t k=length-1;k>=1;k--) pos.push_back(k);
        for(size_t k:pos)
        {
            Iterate(k);
            ComputeNewAbigAndNeighbors(k);
            UpdateWeights(k);
        }
        fwd=!fwd;
        if (!rules.empty()) refValue=abs(integrate());
        tol_e= all_of(Inew.begin(),Inew.end(),[](auto x){return x.empty();}) ? tol_e+1 : 1;
    }

    static cx_mat computeTAElem(cx_mat const& Ak, cx_mat const& A1k)
    {
        cx_mat Ak2=join_cols(Ak,A1k), Qk,R;

        arma::qr_econ(Qk, R, Ak2);
        cx_mat Qk2=Qk.rows(0,Ak.n_rows-1);
        cx_mat Qk3=Qk.rows(Ak.n_rows,Qk.n_rows-1);
        return Qk2*inv(Qk3);
    }

    cx_mat evalLocal(int k, int i) const { return computeTAElem(A[k].slice(i),A1[k]); }

    cx_double eval(MultiIndex id) const
    {
        if (id.size()!=length)
            throw invalid_argument("CrossInterpolationTensor::eval(id) id.size()!=tensor rank ");
        auto prod=one;
        for(size_t k=1;k<=length;k++)
            prod=prod*evalLocal(k,id[k-1]);
        return prod(0,0);
    }

    static cx_cube computeTA(cx_cube const& Tk,cx_mat const& A1k)
    {
        auto Ak=CubeReorder(Tk);
        cx_mat Ak2=join_cols(Ak,A1k), Qk,R;
        arma::qr_econ(Qk, R, Ak2);
        cx_mat Qk2=Qk.rows(0,Ak.n_rows-1);
        cx_mat Qk3=Qk.rows(Ak.n_rows,Qk.n_rows-1);
        cx_mat Tx=Qk2*inv(Qk3);
        return CubeReorderBack(Tx,Tk.n_rows);
    }

    cx_cube computeTA(int k) const { return computeTA(A[k],A1[k]); }

    cx_double IntegrateUsing(const vector<vector<double>>& rule) const
    {
        auto prod=cx_mat(1,1,fill::eye);
        for(size_t k=1;k<=length;k++) {
            auto const& myrule=rule[(k-1)%rule.size()];
            cx_cube TA=computeTA(k);
            prod=prod*integrateUsing(TA,myrule);
        }
        return prod(0,0);
    }

//    cx_double IntegrateUsing(const vector<vector<cx_double>>& rule) const
//    {
//        auto prod=cx_mat(1,1,fill::eye);
//        for(size_t k=1;k<=length;k++) {
//            auto const& myrule=rule[(k-1)%rule.size()];
//            cx_cube TA=evalLocalAll(k);
//            prod=prod*integrateUsing(TA,myrule);
//        }
//        return prod(0,0);
//    }

    cx_double integrate() const
    {
        if (rules.empty()) throw runtime_error("tensorCI::integrate() rules is empty");
        return fwd
                ? ( integrateUsing(A[1],rules[0]) * w2[2] ).eval()(0,0)
                : ( w1[length-1] * integrateUsing(A[length], rules[ (length-1)%rules.size() ]) ).eval()(0,0) ;
    }

//    cx_double IntegrateUsing(const vector<double>& rule) const { return IntegrateUsing(vector<vector<double>>(1,rule)); }
//    cx_double IntegrateUsing(const vector<cx_double>& rule) const { return IntegrateUsing(vector<vector<cx_double>>(1,rule)); }

    double errorEstimation(int nSampling=10, tensorBool fBool={}) const
    {
        double e=0;
        for(int i=0;i<nSampling;i++)
        {
            MultiIndex s(length,0);
            do
                for(size_t k=1;k<=length;k++) s[k-1]=rand()%locISetv[k].size();
            while (fBool && !fBool(s));
            double error=abs(eval(s)-f({s.begin(),s.end()}));
            if (error>e) e=error;
        }
        return e;
    }
    double LastSweepPivotError() const
    {
        if (pivotErrors.empty()) return 0;
        return *max_element(pivotErrors.begin(),pivotErrors.end()) / refValue;
    }
};


#endif // TENSOR_CI_H
