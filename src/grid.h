#ifndef GRID_H
#define GRID_H

#include<vector>
#include<string>
#include<boost/math/quadrature/gauss_kronrod.hpp>


using  stdvec=std::vector<double>;
using std::pair;

template <typename T>
std::vector<T> linspace(T a, T b, size_t n) {
    if (n==0) return {};
    if (n==1) return {a};
    T h = (b - a) / static_cast<T>(n-1);
    std::vector<T> xs(n);
    for(size_t i=0;i<n;i++)
        xs[i]=a+i*h;
    return xs;
}

template <typename T>
std::vector<T> logspace(T a, T b, size_t n) {
    auto xs=linspace<T>(log(a),log(b),n);
    for(auto& x:xs) x=exp(x);
    return xs;
}

template<int nGK=15>
auto QuadratureGL(double a=0, double b=1)
{
    using gl=boost::math::quadrature::gauss<double, nGK>;
    int nq=2*gl::abscissa().size()-1;
    stdvec xi(nq), weight(nq);
    double factor=0.5*(b-a);
    for(uint i=0;i<gl::abscissa().size();i++)
    {
        xi[nq/2+i]=factor*(gl::abscissa()[i]+1)+a;
        weight[nq/2+i]=weight[nq/2-i]=gl::weights()[i]*factor;
        xi[nq/2-i]=factor*(-gl::abscissa()[i]+1)+a;
    }
    return make_pair(xi,weight);
}

template<int nGK=15>
auto QuadratureGK(double a=0, double b=1)
{
    static_assert(nGK%2==1,"GK quadrature requires odd order");
    using gk=boost::math::quadrature::gauss_kronrod<double, nGK>;
    int nq=2*gk::abscissa().size()-1;
    std::vector<double> xi(nq), weight(nq);
    double factor=0.5*(b-a);
    for(uint i=0;i<gk::abscissa().size();i++)
    {
        xi[nq/2+i]=factor*(gk::abscissa()[i]+1)+a;
        weight[nq/2+i]=weight[nq/2-i]=gk::weights()[i]*factor;
        xi[nq/2-i]=factor*(-gk::abscissa()[i]+1)+a;
    }
    return make_pair(xi,weight);
}
#endif // GRID_H
