#ifndef INDEXPACK_H
#define INDEXPACK_H

#include<vector>
#include<map>
#include<string>
#include<iostream>

#include"tools/serialize.hpp"

using namespace std;

template<class IndexType=int>
using MultiIndexG=basic_string<IndexType> ;
using HybIndex=pair<int,double>;

template<class IndexType=int>
class IndexPackG
{
    using MultiIndex=MultiIndexG<IndexType> ;
    vector<MultiIndex> Idirect;
    map<MultiIndex,int> Iinv;

public:
    size_t size() const { return Idirect.size(); }
    auto begin() const { return Idirect.cbegin(); }
    auto end() const { return Idirect.cend(); }

    void push_back(MultiIndex s) { Idirect.push_back(s); Iinv[s]=Iinv.size(); }
    MultiIndex at(int i) const { return Idirect.at(i); }
    int pos(const MultiIndex& s) const { return Iinv.at(s); }
    template<class Container>
    vector<int> pos(const Container& pack) const
    {
        vector<int> out;
        for(const auto& s:pack) out.push_back(pos(s));
        return out;
    }

    MultiIndex toPartial(const MultiIndex& s) const { return {pos(s.substr(0,s.size()-1)),s.back()}; }
    vector<MultiIndex> toPartial(const IndexPackG& pack) const
    {
        vector<MultiIndex> out;
        for(const auto& s:pack) out.push_back(toPartial(s));
        return out;
    }
    MultiIndex toPartialCol(const MultiIndex& s) const { return {s.front(),pos(s.substr(1,s.size()-1))}; }
    vector<MultiIndex> toPartialCol(const IndexPackG& pack) const
    {
        vector<MultiIndex> out;
        for(const auto& s:pack) out.push_back(toPartialCol(s));
        return out;
    }

    HybIndex toHyb(const MultiIndex& s) const { return {pos(s.substr(0,s.size()-1)),s.back()}; }
    vector<HybIndex> toHyb(const IndexPackG& pack) const
    {
        vector<HybIndex> out;
        for(const auto& s:pack) out.push_back(toHyb(s));
        return out;
    }
    HybIndex toHybCol(const MultiIndex& s) const { return {pos(s.substr(1,s.size()-1)),s.front()}; }
    vector<HybIndex> toHybCol(const IndexPackG& pack) const
    {
        vector<HybIndex> out;
        for(const auto& s:pack) out.push_back(toHybCol(s));
        return out;
    }
    void save(ostream& out) const
    {
        auto saveMultiIndex = [](const MultiIndexG<IndexType>& idx, ostream& stream){write_sequence<MultiIndexG<IndexType>, IndexType>(idx, stream, saveNumeric<IndexType>);};
        write_sequence<vector<MultiIndexG<IndexType>>, MultiIndexG<IndexType>>(Idirect, out, saveMultiIndex);
        write_map<MultiIndex, int>(Iinv, out, saveMultiIndex, saveScalar<int>);
    }
    void load(ifstream& in)
    {
        auto readMultIndex = [](ifstream& in){return read_sequence<MultiIndexG<IndexType>, IndexType>(in, readScalar<IndexType>);};
        Idirect = read_sequence<vector<MultiIndexG<IndexType>>, MultiIndexG<IndexType>>(in, readMultIndex);
        Iinv = read_map<MultiIndex, int>(in, readMultIndex, readScalar<int>);
    }
    void print(string msg="") const
    {
        cout<<msg;
        for(size_t i=0;i<Idirect.size();i++)
        {
            for(auto c:Idirect[i]) cout<< c<<" ";
            if (i<Idirect.size()-1) cout<<", ";
        }
    }

    friend IndexPackG kron(IndexPackG I1,IndexPackG I2)
    {
        IndexPackG R;
        for(const auto& s1:I1)
            for(const auto &s2:I2)
                R.push_back(s1+s2);
        return R;
    }
};

template <class T>
IndexPackG<T> LinSpace(T a, T b, int n)
{
    IndexPackG<T> s;
    T dx=(b-a)/(n-1);
    for(int i=0;i<n;i++)
        s.push_back({a+i*dx});
    return s;
}

typedef MultiIndexG<> MultiIndex;
typedef IndexPackG<> IndexPack;
typedef MultiIndexG<double> MultiIndexd;
typedef IndexPackG<double> IndexPackd;

inline MultiIndexd convertMI(MultiIndex const& id,vector<double> const& xs)
{
    MultiIndexd out;
    for(const auto& i:id) out+=xs[i];
    return out;
}

inline IndexPackd convertIP(IndexPack const& Iset,vector<double> const& xs)
{
    IndexPackd out;
    for(MultiIndex const& id:Iset) out.push_back(convertMI(id,xs));
    return out;
}

#endif // INDEXPACK_H
