#ifndef MAT_LAZY_H
#define MAT_LAZY_H

#include<complex>
#include<vector>
#define ARMA_DONT_USE_OPENMP
#include<armadillo>

using namespace std;
using namespace arma;

class MatLazy
{
public:
    typedef function<cx_double(int,int)> mat_fun_t;

    int n_rows=0;
    int n_cols=0;
    mat_fun_t f;
    cx_mat A;

    MatLazy(){}
    MatLazy(mat_fun_t mf,int nrows,int ncols)
        : n_rows(nrows)
        , n_cols(ncols)
        , f(mf)
        , A(nrows,ncols,fill::none)
    { A.fill(datum::inf); }

    void prepareEval(vector<pair<int,int>> const& ids)
    {
        vector<pair<int,int>> ids2;
        for(auto [i,j]:ids)
            if(A(i,j)==datum::inf) ids2.push_back({i,j});
#pragma omp parallel for
        for(size_t i=0;i<ids2.size();i++) {
            auto const& id=ids2[i];
            A(id.first,id.second)=f(id.first,id.second);
        }
    }

    void prepareEvalCross(uvec const& Iset,uvec const& Jset)
    {
        vector<pair<int,int>> ids;
        for(int i:Iset)
            for(int j=0;j<n_cols;j++)
                ids.push_back({i,j});
        for(int i=0;i<n_rows;i++)
            for(int j:Jset)
                ids.push_back({i,j});
        prepareEval(ids);
    }

    void prepareEvalRow(int i) {
        vector<pair<int,int>> ids;
        for(int j=0;j<n_cols;j++)
            ids.push_back({i,j});
        prepareEval(ids);
    }

    void prepareEvalCol(int j) {
        vector<pair<int,int>> ids;
        for(int i=0;i<n_rows;i++)
            ids.push_back({i,j});
        prepareEval(ids);
    }

    const cx_double& operator()(int i,int j) const { return A(i,j); }
    auto cols(uvec const& Jset) const { return A.cols(Jset); }
    auto rows(uvec const& Iset) const { return A.rows(Iset); }
    const cx_mat& matrix() const { return A; }

};

#endif // MAT_LAZY_H
