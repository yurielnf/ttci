#ifndef MATRIX_CI_H
#define MATRIX_CI_H

#include "mat_lazy.h"

enum class PivotSearch {full,alternate,fullWithCondition,alternateWithCondition,alternateEvalWithCondition};
struct PivotData {int i,j; double error;};

struct MatrixCI
{
    MatLazy& Af;
    vector<int> Iset,Jset;
    cx_mat Atci;
    PivotSearch pivotSearch=PivotSearch::alternate;
    int nRandom=1, nIter=5; // for alternate search
    umat fBool; // fBool(i,j)=0 when i,j should not be a pivot
    cx_vec w1,w2;

    PivotData pivotLast;

    MatrixCI(MatLazy& Af_, vector<int> const& Iset_={}, vector<int> const& Jset_={})
        : Af(Af_)
        , Iset(Iset_)
        , Jset(Jset_)
        , Atci(to_mat(Af_, Iset_,Jset_))
    {}

    static cx_mat to_mat(MatLazy &Af,vector<int> const& Iset,vector<int> const& Jset)
    {
        if (Iset.empty())
            return cx_mat(Af.n_rows, Af.n_cols,fill::zeros);
//        return A.cols(Jset) * inv(A1) * A.rows(Iset);
        uvec Iset_=conv_to<uvec>::from(Iset);
        uvec Jset_=conv_to<uvec>::from(Jset);
        cx_mat Q,P;
        qr_econ(Q,P,Af.cols(Jset_));
        return Q * inv(Q.rows(Iset_)) * Af.rows(Iset_) ;
    }

    static vector<int> set_diff(int n, vector<int> Iset)
    {
        vector<int> all(n);
        for(int i=0;i<n;i++) all[i]=i;
        std::sort(Iset.begin(),Iset.end());
        vector<int> diff;
        std::set_difference(all.begin(),all.end(),
                            Iset.begin(),Iset.end(),back_inserter(diff));
        return diff;
    }

    double localError(int i,int j) const
    {
        cx_double err=Af(i,j)-Atci(i,j);
        if (w1.empty() || w2.empty()) return abs(err);
        return abs(err*w1[i]*w2[j]);
    }

    PivotData FindNewPivot() const
    {
        if (pivotSearch > PivotSearch::alternate && fBool.empty())
            throw std::invalid_argument("FindNewPivot with condition: not condition function provided");
        if (pivotSearch==PivotSearch::full || pivotSearch==PivotSearch::fullWithCondition)
            return FindNewPivot(set_diff(Af.n_rows,Iset), set_diff(Af.n_cols,Jset));
        else
            return FindNewPivotAlternate();
    }

    void Iterate()
    {
        pivotLast=FindNewPivot();
        Iset.push_back(pivotLast.i);
        Jset.push_back(pivotLast.j);
        Atci=to_mat(Af,Iset,Jset);
    }

    PivotData FindNewPivotAlternate() const
    {
        auto I0=set_diff(Af.n_rows,Iset);
        auto J0=set_diff(Af.n_cols,Jset);
        int pi=-1, pj=-1;
        double error=0;

        vector<pair<int,int>> ids;
        if (pivotSearch==PivotSearch::alternate)
            for(auto i:I0)
                for(auto j:J0)
                    ids.push_back({i,j});
        else
            for(auto i:I0)
                for(auto j:J0)
                    if(fBool(i,j)==1) ids.push_back({i,j});
        if (ids.empty()) return {pi,pj,0};

        for(int t=0;t<nRandom;t++)
        {
            int j0=ids[rand()%ids.size()].second, i0=-1;
            for(int k=0;k<nIter;k++)
            {
                int inew=FindNewPivot(I0,vector<int>(1,j0)).i;
                int jnew=FindNewPivot(vector<int>(1,inew),J0).j;
                if(i0==inew && j0==jnew) break;// rook condition
                i0=inew; j0=jnew;
            }
            auto e=localError(i0,j0);
            if (t==0 || e>error) { error=e; pi=i0; pj=j0; }
        }
        return {pi,pj,error};
    }

protected:
    PivotData FindNewPivot(vector<int> const& I0,vector<int> const& J0) const
    {
        vector<pair<int,int>> ids;
        if (pivotSearch==PivotSearch::fullWithCondition || pivotSearch==PivotSearch::alternateWithCondition)
        {
            for(auto i:I0)
                for(auto j:J0)
                    if(fBool(i,j)==1) ids.push_back({i,j});
        }
        else
        {
            for(auto i:I0)
                for(auto j:J0)
                    ids.push_back({i,j});
        }
        if (ids.empty()) return {-1,-1,0.};
        Af.prepareEval(ids);            // the idea here is that A(i,j) computes all the ids in parallel
        vector<double> err;
        if (pivotSearch==PivotSearch::alternateEvalWithCondition)
            for(auto [i,j]:ids)
                err.push_back( fBool(i,j)==1 ? localError(i,j) : 0.0 );
        else
            for(auto [i,j]:ids)
                err.push_back( localError(i,j) );
        int p=max_element(err.begin(),err.end())-err.begin();
        return { ids[p].first, ids[p].second, err[p]  };
    }

};

#endif // MATRIX_CI_H
