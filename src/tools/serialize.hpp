#pragma once

#include <fstream>
#include <functional>
#include <armadillo>
#include <iomanip>

using namespace std;
using namespace arma;


template< typename T >
void write_mat(const T & mat, ostream& out){
    out << mat.n_rows << " " << mat.n_cols << "\n";
    for (size_t i=0; i < mat.n_rows; i++){
        for (size_t j=0; j < mat.n_cols; j++)
           out  << setprecision(18) << mat(i, j) << " ";
        out << "\n";
    }
    out << "\n";
}

template< typename S, typename T >
void write_cube(const S & cube, ostream& out){
    out << cube.n_rows << " " << cube.n_cols << " " << cube.n_slices << "\n";
    for (size_t i=0; i < cube.n_slices; i++)
        write_mat<T>(cube.slice(i), out);
}

template< typename S, typename T >
void write_sequence(const S & sequence, ostream& out, function<void(const T&, ostream&)> wfunc){
    out << sequence.size() << "\n";
    for (auto const& x:sequence)
        wfunc(x, out);
    out << "\n";
}

template< typename S, typename T >
void write_map(const map<S, T> & sequence, ostream& out, function<void(const S&, ostream&)> wkey , function<void(const T&, ostream&)> wval){
    out << sequence.size() << "\n";
    for (auto const& [key, val] : sequence){
        wkey(key, out);
        wval(val, out);
    }
    out << "\n";
}

template< typename S, typename T >
S read_sequence(ifstream& in, function<T(ifstream&)> rfunc){
    int size;
    in >> size;
    S sequence;
    for (auto i=0; i < size; i++){
        auto elem = rfunc(in);
        sequence.push_back(elem);
    }
    return sequence;
}

template< typename T >
T read_mat(ifstream& in){
    int n_rows, n_cols;
    in >> n_rows;
    in >> n_cols;
    T mat(n_rows, n_cols);
    for (auto i=0; i < n_rows; i++){
        for (auto j=0; j < n_cols; j++)
           in >> mat(i, j);
    }
    return mat;
}

template< typename S, typename T >
S read_cube(ifstream& in){
    int n_rows, n_cols, n_slices;
    in >> n_rows;
    in >> n_cols;
    in >> n_slices;
    S cube(n_rows, n_cols, n_slices);
    for (auto i=0; i < n_slices; i++)
        cube.slice(i) = read_mat<T>(in);
    return cube;
}

template< typename S, typename T >
map<S, T> read_map(ifstream& in, function<S(ifstream&)> rkey , function<T(ifstream&)> rval){
    map<S, T> a;
    int size;
    in >> size;
    for (int i=0; i < size; i++){
        auto key = rkey(in);
        auto val = rval(in);
        a[key] = val;
    }
    return a;
}

template< typename T >
inline void saveScalar(const T& x, ostream& out){out << x << " ";};

template< typename T >
inline T readScalar(ifstream& in){T x; in >> x; return x;};

template< typename T >
inline void saveNumeric(const T& x, ostream& out){out << setprecision(18) << x << " ";};

// -- some specializations

inline void saveCube(const cx_cube& x, ostream& out){write_cube<cx_cube, cx_mat>(x, out);};
inline void saveMat(const cx_mat& x, ostream& out){write_mat<cx_mat>(x, out);};

inline cx_cube readCube(ifstream& in){return read_cube<cx_cube, cx_mat>(in);};
inline cx_mat readMat(ifstream& in){return read_mat<cx_mat>(in);};
