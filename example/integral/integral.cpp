#include <iostream>
#include <vector>

#include "tensor/ctensor_ci.h"
#include "grid.h"

using namespace std;


int main()
{
    long count=0;
    auto f=[&count](vector<double> xs) {
        count++;
        double x=0, y=0,c=0;
        for(auto xi:xs) {c++; x+=c*xi; y+=xi*xi/c;}
        double arg=1.0+(x+2*y+x*y)*M_PI;
        return cx_double(1+x+cos(arg),x*x+0.5*sin(arg));
    };
    int dim=5;
    auto [xi,wi]=QuadratureGK<15>(0,1);
    auto ci=CTensorCI(dim,xi,f);
    cout<<"rank nEval LastSweepPivotError globalError100 integral(f)\n"<<setprecision(12);
    for(int i=1;i<=120;i++)
    {
        ci.sweepHalf();
        if (i%10==0)
            cout<<i<<" "<<count<<" "<<ci.pivotErrorLastSweep()<<" "<<ci.errorEstimation(100)<<" "<<ci.integrateUsing({wi}).real()<<endl;
    }

    return 0;
}
