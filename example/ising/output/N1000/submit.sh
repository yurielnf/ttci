#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
###SBATCH --nodelist=n03
###SBATCH --output=output.txt    # File name for standard output
###SBATCH --error=error.txt     # File name for standard error output

export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=1

export PATH=/home/kloss/opt/gcc-10.2:$PATH
export CPATH=/home/kloss/opt/gcc-10.2/include:/home/kloss/opt/gsl/include
export CC=/home/kloss/opt/gcc-10.2/bin/gcc
export CXX=/home/kloss/opt/gcc-10.2/bin/g++
export LD_LIBRARY_PATH=/home/kloss/opt/gcc-10.2/lib64:$LD_LIBRARY_PATH
export CMAKE_PREFIX_PATH=/home/kloss/opt/arma:/home/kloss/opt/gsl:/home/yurielnf/.local/lib/python3.7/site-packages/pybind11


time /home/yurielnf/projects/ising1d/build/tci_ising $1 1000 0.01 30
