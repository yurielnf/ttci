// $CXX -std=c++17  prepare_input.cpp

#include<iostream>
#include<fstream>
#include<string>
#include <filesystem>
namespace fs = std::filesystem;

using namespace std;
using namespace std::string_literals;


int main()
{
    int nT=64;
    double T0=1.0, dT=(3.0-T0)/(nT-1);
    
    for(int t=0;t<nT;t++) {
        double T=T0+dT*t;
        string folder="T"+to_string(t); 
        fs::create_directory(folder);
        fs::current_path(folder);
        string cmd="sbatch -J "s+folder+" --output=output.txt ../submit.sh "+to_string(T);
        system(cmd.c_str());
        fs::current_path("..");
    }
    
    return 0;
}
