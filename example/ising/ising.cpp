#include <iostream>
#include <vector>

#include "tensor/tensor_ci.h"

using namespace std;


void TestIsingProblem(int L=100, int S=2,int rankMax=30)
{
    long c=0;
    auto energy=[=](vector<int> const& id) {
        double sum=0;
        for(size_t i=0;i<id.size();i++)
             for(size_t j=0;j<id.size();j++)
                sum+= (id[i]-S)*(id[j]-S)/((i-j)*(i-j)+1.0) ;
        return sum;
    };
    auto ref=vector<int>(L,S);
    auto myTf=[=,&c](vector<int> const& id) { c++; return exp(-energy(id)); };

    time_t t0=time(NULL);
    auto ci=TensorCI(L,2*S+1,myTf,ref);
    ci.pivotSearch=PivotSearch::full; ci.tol=1e-12;
    auto Z=vector(L,vector<double>(2*S+1,1.0)); //normal sum
    cout<<"rank time neval pivotError sum\n";
    for(int i=1;i<=rankMax;i++)
    {
        if (i>1) ci.SweepHalf();
        double z= real( ci.IntegrateUsing(Z) );
        cout<<i<<" "<<difftime(time(NULL),t0)<<" "<<c<<" "<<ci.LastSweepPivotError()<<" "<<z<<endl;
    }
}

int main(int argc, char *argv[])
{
    if (argc==4)
        TestIsingProblem(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
    else
        cout << "usage:\n"<<argv[0]<<" <Nsites> <Spin> <nsweep>" << endl;

    return 0;
}
