#include"catch.hpp"
#include"matrix/matrix_ci.h"

TEST_CASE( "Test matrix CI" )
{
    SECTION( "fixed rank" )
    {
        int n=100, r=50;
        cx_mat A1(n,r,fill::randn), A2(r,n,fill::randn);
        cx_mat A=A1*A2;
        auto Af=MatLazy(A, A.n_rows, A.n_cols);
        auto ci=MatrixCI( Af  );
        for(int i=1;i<r+5;i++)
        {
            ci.Iterate();
            if (i==r)
                REQUIRE( norm(A-ci.Atci) <= 1e-13*norm(A) );
            if (i==r+1)
                REQUIRE( ci.pivotLast.error <= 1e-13*norm(A) );
        }
    }

    SECTION( "continuous function" )
    {
        auto myf=[](double x,double y) {
            double arg=1.0+(x+2*y+x*y)*M_PI;
            return cx_double(1+x+cos(arg),x*x+0.5*sin(arg));
        };
        int n=100,r=15;
        double dx=1.0/n;
        cx_mat A(n,n);
            for(int i=0;i<n;i++)
                for(int j=0;j<n;j++)
                    A(i,j)=myf(i*dx,j*dx);

        auto Af=MatLazy( A, n, n );

        auto ci=MatrixCI(Af);
        ci.pivotSearch=PivotSearch::full; // TODO: fails for alternate
        //cout<<"rank pivotError norm(A-A~)\n";
        for(int i=1;i<=r;i++)
        {
            ci.Iterate();
            //cout<<i<<" "<<ci.pivotLast.error<<" "<<norm(A-ci.Atci)<<endl;
        }
        REQUIRE( norm(A-ci.Atci) <= 1e-13*norm(A) );
        REQUIRE( ci.pivotLast.error <= 1e-13*norm(A) );
    }

}
