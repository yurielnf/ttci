#include"catch.hpp"

#include<iostream>
#include<cmath>
#include"tensor/ctensor_ci.h"
#include"grid.h"

using namespace std;

TEST_CASE( "Test continuous tensor CI" )
{
    SECTION( "Function in Rn" )
    {
        long count=0;
        auto f=[&count](vector<double> xs) {
            count++;
            double x=0, y=0,c=0;
            for(auto xi:xs) {c++; x+=c*xi; y+=xi*xi/c;}
            double arg=1.0+(x+2*y+x*y)*M_PI;
            return cx_double(1+x+cos(arg),x*x+0.5*sin(arg));
        };
        int dim=5;
        auto [xi,wi]=QuadratureGK<15>(0,1);
        auto ci=CTensorCI(dim,xi,f);
        cout<<"rank nEval LastSweepPivotError globalError100 integral(f)\n";
        for(int i=1;i<=120;i++)
        {
            ci.sweepHalf();
            if (i%10==0)
                cout<<i<<" "<<count<<" "<<ci.pivotErrorLastSweep()<<" "<<ci.errorEstimation(100)<<" "<<ci.integrateUsing({wi}).real()<<endl;
        }
        REQUIRE( ci.pivotErrorLastSweep()<1e-6 );
        REQUIRE( ci.errorEstimation(100)<1e-5 );
    }

    SECTION( "Integration" )
    {
        auto f=[](vector<double> xs) {
            double y=1.0;
            for(auto x:xs)
                y *= exp(- x * x);
            return cx_double(y);
        };

        // primitive of f = exp(- x * x)
        auto F=[](double x) {return 0.5 * sqrt(M_PI) * erf(x);};

        int dim=5;
        double a = -1, b = 1;
        auto [xi,wi]=QuadratureGK<41>(a,b);

        auto ci=CTensorCI(dim,xi,f);

        for(int i=0;i<10;i++)
            ci.sweepHalf();

        auto Fx = ci.integrateUsing({wi});
        auto Fx_ref = pow (F(b) - F(a), dim);
        REQUIRE( abs(Fx - Fx_ref) <= 1e-10 );
    }

}
