#include"catch.hpp"

#include<iostream>
#include"tensor/tensor_ci.h"

using namespace std;

TEST_CASE( "Test tensor CI" )
{
    SECTION( "Function in Rn" )
    {
        long count=0;
        auto f=[&count](vector<double> xs) {
            count++;
            double x=0, y=0,c=0;
            for(auto xi:xs) {c++; x+=c*xi; y+=xi*xi/c;}
            double arg=1.0+(x+2*y+x*y)*M_PI;
            return cx_double(1+x+cos(arg),x*x+0.5*sin(arg));
        };
        int dim=5;
        auto xi=::linspace(0.,1.,11);

        auto myTf=[=](vector<int> const& id) {
          vector<double> xs;
          for(auto i:id) xs.push_back(xi[i]);
          return f(xs);
        };
        auto ci=TensorCI(dim,xi.size(),myTf);
        //cout<<"rank nEval LastSweepPivotError globalError100\n";
        for(int i=1;i<=120;i++)
        {
            ci.SweepHalf();
            //if (i%10==0)
            //    cout<<i<<" "<<count<<" "<<ci.LastSweepPivotError()<<" "<<ci.errorEstimation(100)<<endl;
        }
        REQUIRE( ci.LastSweepPivotError()<1e-8 );
        REQUIRE( ci.errorEstimation(100)<1e-8 );
    }
}
