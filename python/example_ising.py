import sys
import numpy as np
sys.path.append("../build")  # set to correct path for module
import ttcipy


def TestIsing(L=20, S=2, rankMax=10):
    def energy(id):
        sum = 0.0
        for i in range(len(id)):
            for j in range(len(id)):
                sum += (id[i]-S)*(id[j]-S)/((i-j)*(i-j)+1)
        return sum

    def myTf(id):
        myTf.c += 1
        return np.exp(-energy(id))
    myTf.c = 0

    ref = [S]*L
    ci = ttcipy.TensorCI(L, 2*S+1, myTf, ref)
    ci.pivotSearch = ttcipy.PivotSearch.full
    Z = [[1]*(2*S+1)]*L
    print("rank neval pivotError sum")
    for i in range(1, rankMax+1):
        if i > 1:
            ci.sweepHalf()
        z = ci.integrateUsing(Z).real
        print(i, myTf.c, ci.pivotError(), z)


TestIsing()
