#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <pybind11/cast.h>

#include "tensor/tensor_ci.h"

using namespace std;

namespace py = pybind11;


PYBIND11_MODULE(ttcipy, m) {
    m.doc() = "Python interface for tensor train cross interpolation (ttci)";


    //----------------- SIAM integrand ------------------------

    py::enum_<PivotSearch>(m, "PivotSearch")
        .value("full", PivotSearch::full)
        .value("alternate", PivotSearch::alternate);

    py::class_<TensorCI>(m, "TensorCI")
            .def(py::init<int,int,TensorF,vector<int>>())
            .def(py::init<vector<int>,TensorF,vector<int>>())
            .def("sweepHalf",  &TensorCI::SweepHalf)
            .def("pivotError",  &TensorCI::LastSweepPivotError)
            .def("integrateUsing", &TensorCI::IntegrateUsing)
            .def_readwrite("pivotSearch",&TensorCI::pivotSearch)
            ;

}

