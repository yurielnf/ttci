# Tensor train cross interpolation

Variable separation to approximate a given tensor or function using tensor train cross interpolation.

## Dependencies

- [armadillo](http://arma.sourceforge.net/)
- [pybind11](https://github.com/pybind/pybind11)

## Installation

```
git clone https://gitlab.kwant-project.org/ttd/ttci.git
cd ttci
mkdir build; cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
sudo make install ## optional
```

## Usage

See the folder `example` and `python`
